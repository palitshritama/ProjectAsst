import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

       result = '';

    btnClicked(btn) {
        if (btn == 'C') {
            this.result = '';
        }
        
        else {
            this.result += btn;
        }
    }

  constructor(public navCtrl: NavController) {

  }

}

   